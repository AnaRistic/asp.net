﻿using McDonalds.Models;
using McDonalds.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace McDonalds.Controllers
{
    public class ArtikliController : ApiController
    {
        private readonly IArtiklRepository _artiklRepository;

        public ArtikliController(IArtiklRepository artiklRepository)
        {
            this._artiklRepository = artiklRepository;
        }

        // GET: api/artikli
        public List<ArtiklModel> Get()
        {
            return this._artiklRepository.Get();
        }

        // GET: api/artikl/id
        [HttpGet]
        [Route("api/artikl/{id}")]
        public HttpResponseMessage Get(int id)
        {
            var artikl = this._artiklRepository.GetById(id);

            if (artikl != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, artikl);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Artikl sa id = " + id.ToString() + " ne postoji");
            }
        }

        // POST: api/artikl
        [HttpPost]
        [Route("api/artikl")]
        public HttpResponseMessage Post([FromBody] ArtiklModel model)
        {
            try
            {
                this._artiklRepository.Add(model);

                var message = Request.CreateResponse(HttpStatusCode.Created, model);
                message.Headers.Location = new Uri(Request.RequestUri + model.Id.ToString());
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // PUT: api/artikl/id
        [HttpPut]
        [Route("api/artikl/{id}")]
        public HttpResponseMessage Put(int id, ArtiklModel model)
        {
            try
            {
                if (this._artiklRepository.Put(id, model) == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Artikl sa id = " + id.ToString() + " ne postoji");
                }
                else
                {
                    var result = this._artiklRepository.Put(id, model);
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

        }

        // DELETE: api/artikl/id
        [HttpDelete]
        [Route("api/artikl/{id}")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                if (this._artiklRepository.Delete(id) == false)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Artikl sa id = " + id.ToString() + " ne postoji");
                }
                else
                {
                    this._artiklRepository.Delete(id);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
