﻿using McDonalds.Models;
using McDonalds.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace McDonalds.Controllers
{
    public class BrojStolovaController : ApiController
    {
        private readonly IBrojStolaRepository _brojStolaRepository;

        public BrojStolovaController(IBrojStolaRepository brojStolaRepository)
        {
            this._brojStolaRepository = brojStolaRepository;
        }

        // GET: api/brojstolova
        public List<BrojStolaModel> Get()
        {
            return this._brojStolaRepository.Get();
        }

        // GET: api/brojstola/id
        [HttpGet]
        [Route("api/brojstola/{id}")]
        public HttpResponseMessage Get(int id)
        {
            var brojStola = this._brojStolaRepository.GetById(id);

            if (brojStola != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, brojStola);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Sto sa id = " + id.ToString() + " ne postoji");
            }
        }

        // POST: api/brojstola
        [HttpPost]
        [Route("api/brojstola")]
        public HttpResponseMessage Post([FromBody] BrojStolaModel model)
        {
            try
            {
                this._brojStolaRepository.Add(model);

                var message = Request.CreateResponse(HttpStatusCode.Created, model);
                message.Headers.Location = new Uri(Request.RequestUri + model.Id.ToString());
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // PUT: api/brojstola/id
        [HttpPut]
        [Route("api/brojstola/{id}")]
        public HttpResponseMessage Put(int id, BrojStolaModel model)
        {
            try
            {
                if (this._brojStolaRepository.Put(id, model) == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Sto sa id = " + id.ToString() + " ne postoji");
                }
                else
                {
                    var result = this._brojStolaRepository.Put(id, model);
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

        }

        // DELETE: api/brojstola/id
        [HttpDelete]
        [Route("api/brojstola/{id}")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                if (this._brojStolaRepository.Delete(id) == false)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Sto sa id = " + id.ToString() + " ne postoji");
                }
                else
                {
                    this._brojStolaRepository.Delete(id);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
