﻿using McDonalds.Models;
using McDonalds.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace McDonalds.Controllers
{
    public class NarudzbineController : ApiController
    {
        private readonly INarudzbinaRepository _narudzbinaRepository;

        public NarudzbineController(INarudzbinaRepository narudzbinaRepository)
        {
            this._narudzbinaRepository = narudzbinaRepository;
        }

        // GET: api/narudzbina
        public List<NarudzbinaModel> Get()
        {
            return this._narudzbinaRepository.Get();
        }

        // GET: api/narudzbina/id
        [HttpGet]
        [Route("api/narudzbina/{id}")]
        public HttpResponseMessage Get(int id)
        {
            var narudzbina = this._narudzbinaRepository.GetById(id);

            if (narudzbina != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, narudzbina);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Narudzbina sa id = " + id.ToString() + " ne postoji");
            }
        }

        // POST: api/narudzbina
        [HttpPost]
        [Route("api/narudzbina")]
        public HttpResponseMessage Post([FromBody] NarudzbinaModel model)
        {
            try
            {
                this._narudzbinaRepository.Add(model);

                var message = Request.CreateResponse(HttpStatusCode.Created, model);
                message.Headers.Location = new Uri(Request.RequestUri + model.Id.ToString());
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // PUT: api/narudzbina/id
        [HttpPut]
        [Route("api/narudzbina/{id}")]
        public HttpResponseMessage Put(int id, NarudzbinaModel model)
        {
            try
            {
                if (this._narudzbinaRepository.Put(id, model) == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Narudzbina sa id = " + id.ToString() + " ne postoji");
                }
                else
                {
                    var result = this._narudzbinaRepository.Put(id, model);
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

        }

        // DELETE: api/narudzbina/id
        [HttpDelete]
        [Route("api/narudzbina/{id}")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                if (this._narudzbinaRepository.Delete(id) == false)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Narudzbina sa id = " + id.ToString() + " ne postoji");
                }
                else
                {
                    this._narudzbinaRepository.Delete(id);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
