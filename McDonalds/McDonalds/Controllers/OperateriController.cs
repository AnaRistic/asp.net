﻿using McDonalds.Models;
using McDonalds.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace McDonalds.Controllers
{
    public class OperateriController : ApiController
    {
        private readonly IOperaterRepository _operaterRepository;

        public OperateriController(IOperaterRepository operaterRepository)
        {
            this._operaterRepository = operaterRepository;
        }

        // GET: api/operateri
        public List<OperaterModel> Get()
        {
            return this._operaterRepository.Get();
        }

        // GET: api/operater/id
        [HttpGet]
        [Route("api/operater/{id}")]
        public HttpResponseMessage Get(int id)
        {
            var operater = this._operaterRepository.GetById(id);

            if (operater != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, operater);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Operater sa id = " + id.ToString() + " ne postoji");
            }
        }

        // POST: api/operater
        [HttpPost]
        [Route("api/operater")]
        public HttpResponseMessage Post([FromBody] OperaterModel model)
        {
            try
            {
                this._operaterRepository.Add(model);

                var message = Request.CreateResponse(HttpStatusCode.Created, model);
                message.Headers.Location = new Uri(Request.RequestUri + model.Id.ToString());
                return message;
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // PUT: api/operater/id
        [HttpPut]
        [Route("api/operater/{id}")]
        public HttpResponseMessage Put(int id, OperaterModel model)
        {
            try
            {
                if (this._operaterRepository.Put(id, model) == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Operater sa id = " + id.ToString() + " ne postoji");
                }
                else
                {
                    var result = this._operaterRepository.Put(id, model);
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
            
        }

        // DELETE: api/operater/id
        [HttpDelete]
        [Route("api/operater/{id}")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                if (this._operaterRepository.Delete(id) == false)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Operater sa id = " + id.ToString() + " ne postoji");
                }
                else
                {
                    this._operaterRepository.Delete(id);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
