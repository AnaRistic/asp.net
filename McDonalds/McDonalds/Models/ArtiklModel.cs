﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace McDonalds.Models
{
    public class ArtiklModel
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public Nullable<float> Cena { get; set; }
    }
}