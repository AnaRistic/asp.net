﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace McDonalds.Models
{
    public class NarudzbinaModel
    {
        public int Id { get; set; }
        public Nullable<int> Kolicina { get; set; }
        public int Id_artikla_fk { get; set; }
        public int Id_operatera_fk { get; set; }
        public int Broj_stola_fk { get; set; }
    }
}