﻿using McDonalds.Models;
using McDonalds.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace McDonalds.Repository
{
    public class ArtiklRepository : BaseRepository, IArtiklRepository
    {
        public ArtiklRepository(mc_donaldsEntities context) : base(context)
        {

        }

        public List<ArtiklModel> Get()
        {
            var artikli = context.artikli;
            List<ArtiklModel> listaArtikla = new List<ArtiklModel>();

            foreach (var artikl in artikli)
            {
                ArtiklModel model = new ArtiklModel();
                model.Id = artikl.Id;
                model.Naziv = artikl.Naziv;
                model.Cena = artikl.Cena;
                listaArtikla.Add(model);
            }

            return listaArtikla;
        }

        public ArtiklModel GetById(int id)
        {
            var artikl = context.artikli.FirstOrDefault(o => o.Id == id);

            if (artikl != null)
            {
                ArtiklModel model = new ArtiklModel();
                model.Id = artikl.Id;
                model.Naziv = artikl.Naziv;
                model.Cena = artikl.Cena;

                return model;
            }
            else
            {
                return null;
            }
        }

        public void Add(ArtiklModel model)
        {
            artikli artikl = new artikli();

            artikl.Id = model.Id;
            artikl.Naziv = model.Naziv;
            artikl.Cena = model.Cena;

            context.artikli.Add(artikl);
            context.SaveChanges();
        }

        public ArtiklModel Put(int id, ArtiklModel model)
        {
            var artikl = context.artikli.FirstOrDefault(o => o.Id == id);

            if (artikl == null)
            {
                return null;
            }
            else
            {
                artikl.Id = model.Id;
                artikl.Naziv = model.Naziv;
                artikl.Cena = model.Cena;

                context.SaveChanges();
                return model;
            }
        }

        public bool Delete(int id)
        {
            var artikl = context.artikli.FirstOrDefault(o => o.Id == id);
            if (artikl == null)
            {
                return false;
            }
            else
            {
                context.artikli.Remove(artikl);
                context.SaveChanges();
                return true;
            }
        }
    }
}