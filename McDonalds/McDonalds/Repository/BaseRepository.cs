﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace McDonalds.Repository
{
    public class BaseRepository
    {
        protected readonly mc_donaldsEntities context;

        public BaseRepository(mc_donaldsEntities context)
        {
            this.context = context;
        }
    }
}