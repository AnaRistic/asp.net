﻿using McDonalds.Models;
using McDonalds.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace McDonalds.Repository
{
    public class BrojStolaRepository : BaseRepository, IBrojStolaRepository
    {
        public BrojStolaRepository(mc_donaldsEntities context) : base(context)
        {

        }

        public List<BrojStolaModel> Get()
        {
            var brojStolova = context.broj_stola;
            List<BrojStolaModel> listaBrojaStolova = new List<BrojStolaModel>();

            foreach (var brojStola in brojStolova)
            {
                BrojStolaModel model = new BrojStolaModel();
                model.Id = brojStola.Id;
                model.zauzet = brojStola.zauzet;
                listaBrojaStolova.Add(model);
            }

            return listaBrojaStolova;
        }

        public BrojStolaModel GetById(int id)
        {
            var brojStola = context.broj_stola.FirstOrDefault(o => o.Id == id);

            if (brojStola != null)
            {
                BrojStolaModel model = new BrojStolaModel();
                model.Id = brojStola.Id;
                model.zauzet = brojStola.zauzet;

                return model;
            }
            else
            {
                return null;
            }
        }

        public void Add(BrojStolaModel model)
        {
            broj_stola brojStola = new broj_stola();

            brojStola.Id = model.Id;
            brojStola.zauzet = model.zauzet;

            context.broj_stola.Add(brojStola);
            context.SaveChanges();
        }

        public BrojStolaModel Put(int id, BrojStolaModel model)
        {
            var brojStola = context.broj_stola.FirstOrDefault(o => o.Id == id);

            if (brojStola == null)
            {
                return null;
            }
            else
            {
                brojStola.Id = model.Id;
                brojStola.zauzet = model.zauzet;

                context.SaveChanges();
                return model;
            }
        }

        public bool Delete(int id)
        {
            var brojStola = context.broj_stola.FirstOrDefault(o => o.Id == id);
            if (brojStola == null)
            {
                return false;
            }
            else
            {
                context.broj_stola.Remove(brojStola);
                context.SaveChanges();
                return true;
            }
        }
    }
}