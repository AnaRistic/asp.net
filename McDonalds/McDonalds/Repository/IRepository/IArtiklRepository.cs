﻿using McDonalds.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McDonalds.Repository.IRepository
{
    public interface IArtiklRepository
    {
        List<ArtiklModel> Get();
        ArtiklModel GetById(int id);
        void Add(ArtiklModel model);
        ArtiklModel Put(int id, ArtiklModel model);
        bool Delete(int id);
    }
}
