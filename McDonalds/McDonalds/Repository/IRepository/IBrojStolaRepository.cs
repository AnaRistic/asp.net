﻿using McDonalds.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McDonalds.Repository.IRepository
{
    public interface IBrojStolaRepository
    {
        List<BrojStolaModel> Get();
        BrojStolaModel GetById(int id);
        void Add(BrojStolaModel model);
        BrojStolaModel Put(int id, BrojStolaModel model);
        bool Delete(int id);
    }
}
