﻿using McDonalds.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McDonalds.Repository.IRepository
{
    public interface INarudzbinaRepository
    {
        List<NarudzbinaModel> Get();
        NarudzbinaModel GetById(int id);
        void Add(NarudzbinaModel model);
        NarudzbinaModel Put(int id, NarudzbinaModel model);
        bool Delete(int id);
    }
}
