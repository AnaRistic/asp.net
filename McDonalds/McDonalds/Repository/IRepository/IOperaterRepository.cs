﻿using McDonalds.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McDonalds.Repository.IRepository
{
    public interface IOperaterRepository
    {
        List<OperaterModel> Get();
        OperaterModel GetById(int id);
        void Add(OperaterModel model);
        OperaterModel Put(int id, OperaterModel model);
        bool Delete(int id);
    }
}
