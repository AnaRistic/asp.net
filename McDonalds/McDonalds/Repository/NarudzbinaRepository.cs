﻿using McDonalds.Models;
using McDonalds.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace McDonalds.Repository
{
    public class NarudzbinaRepository : BaseRepository, INarudzbinaRepository
    {
        public NarudzbinaRepository(mc_donaldsEntities context) : base(context)
        {

        }

        public List<NarudzbinaModel> Get()
        {
            var narudzbine = context.narudzbina;
            List<NarudzbinaModel> listaNarudzbina = new List<NarudzbinaModel>();

            foreach (var narudzbina in narudzbine)
            {
                NarudzbinaModel model = new NarudzbinaModel();
                model.Id = narudzbina.Id;
                model.Kolicina = narudzbina.Kolicina;
                model.Id_artikla_fk = narudzbina.Id_artikla_fk;
                model.Id_operatera_fk = narudzbina.Id_operatera_fk;
                model.Broj_stola_fk = narudzbina.Broj_stola_fk;
                listaNarudzbina.Add(model);
            }

            return listaNarudzbina;
        }

        public NarudzbinaModel GetById(int id)
        {
            var narudzbina = context.narudzbina.FirstOrDefault(o => o.Id == id);

            if (narudzbina != null)
            {
                NarudzbinaModel model = new NarudzbinaModel();
                model.Id = narudzbina.Id;
                model.Kolicina = narudzbina.Kolicina;
                model.Id_artikla_fk = narudzbina.Id_artikla_fk;
                model.Id_operatera_fk = narudzbina.Id_operatera_fk;
                model.Broj_stola_fk = narudzbina.Broj_stola_fk;

                return model;
            }
            else
            {
                return null;
            }
        }

        public void Add(NarudzbinaModel model)
        {
            narudzbina narudzbina = new narudzbina();
            narudzbina.Id = model.Id;
            narudzbina.Kolicina = model.Kolicina;
            narudzbina.Id_artikla_fk = model.Id_artikla_fk;
            narudzbina.Id_operatera_fk = model.Id_operatera_fk;
            narudzbina.Broj_stola_fk = model.Broj_stola_fk;

            context.narudzbina.Add(narudzbina);
            context.SaveChanges();
        }

        public NarudzbinaModel Put(int id, NarudzbinaModel model)
        {
            var narudzbina = context.narudzbina.FirstOrDefault(o => o.Id == id);

            if (narudzbina == null)
            {
                return null;
            }
            else
            {
                narudzbina.Id = model.Id;
                narudzbina.Kolicina = model.Kolicina;
                narudzbina.Id_artikla_fk = model.Id_artikla_fk;
                narudzbina.Id_operatera_fk = model.Id_operatera_fk;
                narudzbina.Broj_stola_fk = model.Broj_stola_fk;

                context.SaveChanges();
                return model;
            }
        }

        public bool Delete(int id)
        {
            var narudzbina = context.narudzbina.FirstOrDefault(o => o.Id == id);
            if (narudzbina == null)
            {
                return false;
            }
            else
            {
                context.narudzbina.Remove(narudzbina);
                context.SaveChanges();
                return true;
            }
        }
    }
}