﻿using McDonalds.Models;
using McDonalds.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace McDonalds.Repository
{
    public class OperaterRepository : BaseRepository, IOperaterRepository
    {
        public OperaterRepository(mc_donaldsEntities context) : base(context)
        {

        }

        public List<OperaterModel> Get()
        {
            var operateri = context.operater;
            List<OperaterModel> listaOperatera = new List<OperaterModel>();

            foreach(var operater in operateri)
            {
                OperaterModel model = new OperaterModel();
                model.Id = operater.Id;
                model.Ime = operater.Ime;
                model.Prezime = operater.Prezime;
                listaOperatera.Add(model);
            }

            return listaOperatera;
        }

        public OperaterModel GetById(int id)
        {
            var operater = context.operater.FirstOrDefault(o => o.Id == id);

            if(operater != null)
            {
                OperaterModel model = new OperaterModel();
                model.Id = operater.Id;
                model.Ime = operater.Ime;
                model.Prezime = operater.Prezime;

                return model;
            }
            else
            {
                return null;
            }
        }

        public void Add(OperaterModel model)
        {
            operater operater = new operater();

            operater.Id = model.Id;
            operater.Ime = model.Ime;
            operater.Prezime = model.Prezime;

            context.operater.Add(operater);
            context.SaveChanges();
        }

        public OperaterModel Put(int id, OperaterModel model)
        {
            var operater = context.operater.FirstOrDefault(o => o.Id == id);

            if(operater == null)
            {
                return null;
            }
            else
            {
                operater.Id = model.Id;
                operater.Ime = model.Ime;
                operater.Prezime = model.Prezime;

                context.SaveChanges();
                return model;
            }
        }

        public bool Delete(int id)
        {
            var operater = context.operater.FirstOrDefault(o => o.Id == id);
            if(operater == null)
            {
                return false;
            }
            else
            {
                context.operater.Remove(operater);
                context.SaveChanges();
                return true;
            }
        }
    }
}