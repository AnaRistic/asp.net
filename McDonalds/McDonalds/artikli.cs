//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace McDonalds
{
    using System;
    using System.Collections.Generic;
    
    public partial class artikli
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public artikli()
        {
            this.narudzbina = new HashSet<narudzbina>();
        }
    
        public int Id { get; set; }
        public string Naziv { get; set; }
        public Nullable<float> Cena { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<narudzbina> narudzbina { get; set; }
    }
}
