﻿using System.Web;
using System.Web.Optimization;

namespace MvcTrgovac
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
            "~/bower_components/jquery-ui/jquery-ui.min.js"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
              "~/bower_components/jquery-ui/base/core.css",
              "~/bower_components/jquery-ui/themes/base/resizable.css",
              "~/bower_components/jquery-ui/themes/base/selectable.css",
              "~/bower_components/jquery-ui/themes/base/accordion.css",
              "~/bower_components/jquery-ui/themes/base/autocomplete.css",
              "~/bower_components/jquery-ui/themes/base/button.css",
              "~/bower_components/jquery-ui/themes/base/dialog.css",
              "~/bower_components/jquery-ui/themes/base/slider.css",
              "~/bower_components/jquery-ui/themes/base/tabs.css",
              "~/bower_components/jquery-ui/themes/base/datepicker.css",
              "~/bower_components/jquery-ui/themes/base/progressbar.css",
              "~/bower_components/jquery-ui/themes/base/theme.css"));
            

        }
    }
}
