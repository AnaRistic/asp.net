[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(MvcTrgovac.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(MvcTrgovac.App_Start.NinjectWebCommon), "Stop")]

namespace MvcTrgovac.App_Start
{
    using System;
    using System.Web;

    using System.Data.Entity;
    using MvcTrgovac.Models;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;

    using MvcTrgovac.Repository.IRepository;
    using MvcTrgovac.Repository;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<DbContext>().To<Context>().InRequestScope();
            kernel.Bind<ITrgovacNaMaloRepository>().To<TrgovacNaMaloRepository>().InRequestScope();
            kernel.Bind<ITrgovacNaVelikoRepository>().To<TrgovacNaVelikoRepository>().InRequestScope();
            kernel.Bind<IKupljeniProizvodiPPRepository>().To<KupljeniProizvodiPPRepository>().InRequestScope();
            kernel.Bind<IProdatiProizvodiPPRepository>().To<ProdatiProizvodiPPRepository>().InRequestScope();
            kernel.Bind<IStanjeZalihaPPRepository>().To<StanjeZalihaPPRepository>().InRequestScope();
            kernel.Bind<IKupljeniProizvodiURepository>().To<KupljeniProizvodiURepository>().InRequestScope();
            kernel.Bind<IProdatiProizvodiURepository>().To<ProdatiProizvodiURepository>().InRequestScope();
            kernel.Bind<IStanjeZalihaURepository>().To<StanjeZalihaURepository>().InRequestScope();
        }        
    }
}
