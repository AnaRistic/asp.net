﻿using MvcTrgovac.Models;
using MvcTrgovac.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace MvcTrgovac.Controllers
{
    public class KupljeniProizvodiPPController : Controller
    {
        private readonly IKupljeniProizvodiPPRepository _kupljeniProizvodiPPRepository;

        public KupljeniProizvodiPPController(IKupljeniProizvodiPPRepository kupljeniProizvodiPPRepository)
        {
            this._kupljeniProizvodiPPRepository = kupljeniProizvodiPPRepository;
        }

        // GET: KupljeniProizvodiPP
        public ActionResult Index(string sortOrder, string currentFilter, int? page, string searchString, string SifraVDP, string SifraRM, string SifraJM, string KolicinaKupjenihPP)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.SvdpSortParm = String.IsNullOrEmpty(sortOrder) ? "svdp_desc" : "";
            ViewBag.SrmSortParm = sortOrder == "srm_asc" ? "srm_desc" : "srm_asc";
            ViewBag.SjmSortParm = sortOrder == "sjm_asc" ? "sjm_desc" : "sjm_asc";
            ViewBag.SkolSortParm = sortOrder == "skol_asc" ? "skol_desc" : "skol_asc";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            IQueryable<MasterModel> result = this._kupljeniProizvodiPPRepository.GetAll(searchString, SifraVDP, SifraRM, SifraJM, KolicinaKupjenihPP);

            switch (sortOrder)
            {
                case "svdp_desc":
                    result = result.OrderByDescending(s => s.SifraVDP);
                    break;
                case "srm_asc":
                    result = result.OrderBy(s => s.SifraRM);
                    break;
                case "srm_desc":
                    result = result.OrderByDescending(s => s.SifraRM);
                    break;
                case "sjm_asc":
                    result = result.OrderBy(s => s.SifraJM);
                    break;
                case "sjm_desc":
                    result = result.OrderByDescending(s => s.SifraJM);
                    break;
                case "skol_asc":
                    result = result.OrderBy(s => s.KolicinaKupjenihPP);
                    break;
                case "skol_desc":
                    result = result.OrderByDescending(s => s.KolicinaKupjenihPP);
                    break;
                default:  // Name ascending 
                    result = result.OrderBy(s => s.SifraVDP);
                    break;
            }

            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(result.ToPagedList(pageNumber, pageSize));
        }

        // GET: KupljeniProizvodiPP/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: KupljeniProizvodiPP/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SifraVDP, SifraRM, SifraJM, KolicinaKupjenihPP")] MasterModel masterModel)
        {
            if (ModelState.IsValid)
            {
                _kupljeniProizvodiPPRepository.Create(masterModel);
                return RedirectToAction("Index");
            }
            return View(masterModel);
        }

        // GET: KupljeniProizvodiPP/Edit/id
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasterModel kupljeniProizPP = this._kupljeniProizvodiPPRepository.GetById((int)id);
            if (kupljeniProizPP == null)
            {
                return HttpNotFound();
            }
            return View(kupljeniProizPP);
        }

        // POST: KupljeniProizvodiPP/Edit/id
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MasterModel masterModel)
        {
            if (ModelState.IsValid)
            {
                this._kupljeniProizvodiPPRepository.Edit(masterModel);
                return RedirectToAction("Index");
            }
            return View(masterModel);
        }

        // GET: KupljeniProizvodiPP/Delete/id
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasterModel kupljeniProizPP = this._kupljeniProizvodiPPRepository.GetById((int)id);
            if (kupljeniProizPP == null)
            {
                return HttpNotFound();
            }
            return View(kupljeniProizPP);
        }

        // POST: KupljeniProizvodiPP/Delete/id
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            this._kupljeniProizvodiPPRepository.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this._kupljeniProizvodiPPRepository.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}