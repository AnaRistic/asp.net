﻿using MvcTrgovac.Models;
using MvcTrgovac.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace MvcTrgovac.Controllers
{
    public class KupljeniProizvodiUController : Controller
    {
        private readonly IKupljeniProizvodiURepository _kupljeniProizvodiGRepository;

        public KupljeniProizvodiUController(IKupljeniProizvodiURepository kupljeniProizvodiGRepository)
        {
            this._kupljeniProizvodiGRepository = kupljeniProizvodiGRepository;
        }

        // GET: KupljeniProizvodiG
        public ActionResult Index(string sortOrder, string currentFilter, int? page, string searchString, string SifraVDP, string SifraRM, string SifraJM, string KolicinaKupjenihPP, string KolicinKupljenihDP, string KolicinaKupljenihU)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.SvdpSortParm = String.IsNullOrEmpty(sortOrder) ? "svdp_desc" : "";
            ViewBag.SrmSortParm = sortOrder == "srm_asc" ? "srm_desc" : "srm_asc";
            ViewBag.SjmSortParm = sortOrder == "sjm_asc" ? "sjm_desc" : "sjm_asc";
            ViewBag.SkolPSortParm = sortOrder == "skolp_asc" ? "skolp_desc" : "skolp_asc";
            ViewBag.SkolDSortParm = sortOrder == "skold_asc" ? "skold_desc" : "skold_asc";
            ViewBag.SkolUSortParm = sortOrder == "skolu_asc" ? "skolu_desc" : "skolu_asc";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            IQueryable<MasterModel> result = this._kupljeniProizvodiGRepository.GetAll(searchString, SifraVDP, SifraRM, SifraJM, KolicinaKupjenihPP, KolicinKupljenihDP, KolicinaKupljenihU);

            switch (sortOrder)
            {
                case "svdp_desc":
                    result = result.OrderByDescending(s => s.SifraVDP);
                    break;
                case "srm_asc":
                    result = result.OrderBy(s => s.SifraRM);
                    break;
                case "srm_desc":
                    result = result.OrderByDescending(s => s.SifraRM);
                    break;
                case "sjm_asc":
                    result = result.OrderBy(s => s.SifraJM);
                    break;
                case "sjm_desc":
                    result = result.OrderByDescending(s => s.SifraJM);
                    break;
                case "skolp_asc":
                    result = result.OrderBy(s => s.KolicinaKupjenihPP);
                    break;
                case "skolp_desc":
                    result = result.OrderByDescending(s => s.KolicinaKupjenihPP);
                    break;
                case "skold_asc":
                    result = result.OrderBy(s => s.KolicinaKupjenihPP);
                    break;
                case "skold_desc":
                    result = result.OrderByDescending(s => s.KolicinaKupjenihPP);
                    break;
                case "skolu_asc":
                    result = result.OrderBy(s => s.KolicinaKupjenihPP);
                    break;
                case "skolu_desc":
                    result = result.OrderByDescending(s => s.KolicinaKupjenihPP);
                    break;
                default:  // Name ascending 
                    result = result.OrderBy(s => s.SifraVDP);
                    break;
            }

            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(result.ToPagedList(pageNumber, pageSize));
        }

        // GET: KupljeniProizvodiG/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: KupljeniProizvodiG/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,SifraVDP,SifraRM,SifraJM,KolicinaKupjenihPP,KolicinKupljenihDP")] MasterModel masterModel)
        {
            if (ModelState.IsValid)
            {
                _kupljeniProizvodiGRepository.Create(masterModel);
                return RedirectToAction("Index");
            }
            return View(masterModel);
        }

        // GET: KupljeniProizvodiG/Edit/id
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasterModel kupljeniProizG = this._kupljeniProizvodiGRepository.GetById((int)id);
            if (kupljeniProizG == null)
            {
                return HttpNotFound();
            }
            return View(kupljeniProizG);
        }

        // POST: KupljeniProizvodiG/Edit/id
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MasterModel masterModel)
        {
            if (ModelState.IsValid)
            {
                this._kupljeniProizvodiGRepository.Edit(masterModel);
                return RedirectToAction("Index");
            }
            return View(masterModel);
        }

        // GET: KupljeniProizvodiG/Delete/id
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasterModel kupljeniProizG = this._kupljeniProizvodiGRepository.GetById((int)id);
            if (kupljeniProizG == null)
            {
                return HttpNotFound();
            }
            return View(kupljeniProizG);
        }

        // POST: KupljeniProizvodiG/Delete/id
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            this._kupljeniProizvodiGRepository.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this._kupljeniProizvodiGRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}