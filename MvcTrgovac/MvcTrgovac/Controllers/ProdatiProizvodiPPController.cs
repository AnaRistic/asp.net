﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcTrgovac.Models;
using MvcTrgovac.Repository.IRepository;
using PagedList;

namespace MvcTrgovac.Controllers
{
    public class ProdatiProizvodiPPController : Controller
    {
        private readonly IProdatiProizvodiPPRepository _prodatiProizvodiPPRepository;

        public ProdatiProizvodiPPController(IProdatiProizvodiPPRepository prodatiProizvodiPPRepository)
        {
            this._prodatiProizvodiPPRepository = prodatiProizvodiPPRepository;
        }

        // GET: ProdatiProizvodiPP
        public ActionResult Index(string sortOrder, string currentFilter, int? page, string searchString, string SifraVDP, string SifraRM, string SifraJM, string KolicinaProdatihPP)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.SvdpSortParm = String.IsNullOrEmpty(sortOrder) ? "svdp_desc" : "";
            ViewBag.SrmSortParm = sortOrder == "srm_asc" ? "srm_desc" : "srm_asc";
            ViewBag.SjmSortParm = sortOrder == "sjm_asc" ? "sjm_desc" : "sjm_asc";
            ViewBag.SprodSortParm = sortOrder == "sprod_asc" ? "sprod_desc" : "sprod_asc";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            IQueryable<MasterModel> result = this._prodatiProizvodiPPRepository.GetAll(searchString, SifraVDP, SifraRM, SifraJM, KolicinaProdatihPP);

            switch (sortOrder)
            {
                case "svdp_desc":
                    result = result.OrderByDescending(s => s.SifraVDP);
                    break;
                case "srm_asc":
                    result = result.OrderBy(s => s.SifraRM);
                    break;
                case "srm_desc":
                    result = result.OrderByDescending(s => s.SifraRM);
                    break;
                case "sjm_asc":
                    result = result.OrderBy(s => s.SifraJM);
                    break;
                case "sjm_desc":
                    result = result.OrderByDescending(s => s.SifraJM);
                    break;
                case "sprod_asc":
                    result = result.OrderBy(s => s.KolicinaProdatihPP);
                    break;
                case "sprod_desc":
                    result = result.OrderByDescending(s => s.KolicinaProdatihPP);
                    break;
                default:  // Name ascending 
                    result = result.OrderBy(s => s.SifraVDP);
                    break;
            }

            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(result.ToPagedList(pageNumber, pageSize));
        }

        // GET: ProdatiProizvodiPP/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProdatiProizvodiPP/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SifraVDP, SifraRM, SifraJM, KolicinaProdatihPP")] MasterModel masterModel)
        {
            if (ModelState.IsValid)
            {
                this._prodatiProizvodiPPRepository.Create(masterModel);
                return RedirectToAction("Index");
            }
            return View(masterModel);
        }

        // GET: ProdatiProizvodiPP/Edit/id
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasterModel prodatiProizPP = this._prodatiProizvodiPPRepository.GetById((int)id);
            if (prodatiProizPP == null)
            {
                return HttpNotFound();
            }
            return View(prodatiProizPP);
        }

        // POST: ProdatiProizvodiPP/Edit/id
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MasterModel masterModel)
        {
            if (ModelState.IsValid)
            {
                this._prodatiProizvodiPPRepository.Edit(masterModel);
                return RedirectToAction("Index");
            }
            return View(masterModel);
        }

        // GET: ProdatiProizvodiPP/Delete/id
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasterModel kupljeniProizPP = this._prodatiProizvodiPPRepository.GetById((int)id);
            if (kupljeniProizPP == null)
            {
                return HttpNotFound();
            }
            return View(kupljeniProizPP);
        }

        // POST: ProdatiProizvodiPP/Delete/id
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            this._prodatiProizvodiPPRepository.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this._prodatiProizvodiPPRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}