﻿using MvcTrgovac.Models;
using MvcTrgovac.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace MvcTrgovac.Controllers
{
    public class ProdatiProizvodiUController : Controller
    {
        private readonly IProdatiProizvodiURepository _prodatiProizvodiURepository;

        public ProdatiProizvodiUController(IProdatiProizvodiURepository prodatiProizvodiURepository)
        {
            this._prodatiProizvodiURepository = prodatiProizvodiURepository;
        }

        // GET: ProdatiProizvodiU
        public ActionResult Index(string sortOrder, string currentFilter, int? page, string searchString, string SifraVDP, string SifraRM, string SifraJM, string KolicinaProdatihPP, string KolicinaProdatihDP, string KolicinaProdatihU)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.SvdpSortParm = String.IsNullOrEmpty(sortOrder) ? "svdp_desc" : "";
            ViewBag.SrmSortParm = sortOrder == "srm_asc" ? "srm_desc" : "srm_asc";
            ViewBag.SjmSortParm = sortOrder == "sjm_asc" ? "sjm_desc" : "sjm_asc";
            ViewBag.SprodPSortParm = sortOrder == "sprodp_asc" ? "sprodp_desc" : "sprodp_asc";
            ViewBag.SprodDSortParm = sortOrder == "sprodd_asc" ? "sprodd_desc" : "sprodd_asc";
            ViewBag.SprodUSortParm = sortOrder == "sprodu_asc" ? "sprodu_desc" : "sprodu_asc";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            IQueryable<MasterModel> result = this._prodatiProizvodiURepository.GetAll(searchString, SifraVDP, SifraRM, SifraJM, KolicinaProdatihPP, KolicinaProdatihDP, KolicinaProdatihU);

            switch (sortOrder)
            {
                case "svdp_desc":
                    result = result.OrderByDescending(s => s.SifraVDP);
                    break;
                case "srm_asc":
                    result = result.OrderBy(s => s.SifraRM);
                    break;
                case "srm_desc":
                    result = result.OrderByDescending(s => s.SifraRM);
                    break;
                case "sjm_asc":
                    result = result.OrderBy(s => s.SifraJM);
                    break;
                case "sjm_desc":
                    result = result.OrderByDescending(s => s.SifraJM);
                    break;
                case "sprodp_asc":
                    result = result.OrderBy(s => s.KolicinaProdatihPP);
                    break;
                case "sprodp_desc":
                    result = result.OrderByDescending(s => s.KolicinaProdatihPP);
                    break;
                case "sprodd_asc":
                    result = result.OrderBy(s => s.KolicinaProdatihPP);
                    break;
                case "sprodd_desc":
                    result = result.OrderByDescending(s => s.KolicinaProdatihPP);
                    break;
                case "sprodu_asc":
                    result = result.OrderBy(s => s.KolicinaProdatihPP);
                    break;
                case "sprodu_desc":
                    result = result.OrderByDescending(s => s.KolicinaProdatihPP);
                    break;
                default:  // Name ascending 
                    result = result.OrderBy(s => s.SifraVDP);
                    break;
            }

            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(result.ToPagedList(pageNumber, pageSize));
        }

        // GET: ProdatiProizvodiU/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProdatiProizvodiU/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,SifraVDP,SifraRM,SifraJM,KolicinaProdatihPP,KolicinaProdatihDP")] MasterModel masterModel)
        {
            if (ModelState.IsValid)
            {
                this._prodatiProizvodiURepository.Create(masterModel);
                return RedirectToAction("Index");
            }
            return View(masterModel);
        }

        // GET: ProdatiProizvodiU/Edit/id
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasterModel prodatiProizU = this._prodatiProizvodiURepository.GetById((int)id);
            if (prodatiProizU == null)
            {
                return HttpNotFound();
            }
            return View(prodatiProizU);
        }

        // POST: ProdatiProizvodiU/Edit/id
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MasterModel masterModel)
        {
            if (ModelState.IsValid)
            {
                this._prodatiProizvodiURepository.Edit(masterModel);
                return RedirectToAction("Index");
            }
            return View(masterModel);
        }

        // GET: ProdatiProizvodiU/Delete/id
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasterModel kupljeniProizU = this._prodatiProizvodiURepository.GetById((int)id);
            if (kupljeniProizU == null)
            {
                return HttpNotFound();
            }
            return View(kupljeniProizU);
        }

        // POST: ProdatiProizvodiPP/Delete/id
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            this._prodatiProizvodiURepository.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this._prodatiProizvodiURepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}