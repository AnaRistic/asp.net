﻿using MvcTrgovac.Models;
using MvcTrgovac.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace MvcTrgovac.Controllers
{
    public class StanjeZalihaPPController : Controller
    {
        private readonly IStanjeZalihaPPRepository _stanjeZalihaPPRepository;

        public StanjeZalihaPPController(IStanjeZalihaPPRepository stanjeZalihaPPRepository)
        {
            this._stanjeZalihaPPRepository = stanjeZalihaPPRepository;
        }

        // GET: StanjeZalihaPP
        public ActionResult Index(string sortOrder, string currentFilter, int? page, string searchString, string SifraVDP, string SifraRM, string SifraJM, string StZalihaPocPolugodja, string StZalihaKrPolugodja)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.SvdpSortParm = String.IsNullOrEmpty(sortOrder) ? "svdp_desc" : "";
            ViewBag.SrmSortParm = sortOrder == "srm_asc" ? "srm_desc" : "srm_asc";
            ViewBag.SjmSortParm = sortOrder == "sjm_asc" ? "sjm_desc" : "sjm_asc";
            ViewBag.SzalhPPSortParm = sortOrder == "szalhpp_asc" ? "szalhpp_desc" : "szalhpp_asc";
            ViewBag.SzalhKPSortParm = sortOrder == "szalhkp_asc" ? "szalhkp_desc" : "szalhkp_asc";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            IQueryable<MasterModel> result = this._stanjeZalihaPPRepository.GetAll(searchString, SifraVDP, SifraRM, SifraJM, StZalihaPocPolugodja, StZalihaKrPolugodja);

            switch (sortOrder)
            {
                case "svdp_desc":
                    result = result.OrderByDescending(s => s.SifraVDP);
                    break;
                case "srm_asc":
                    result = result.OrderBy(s => s.SifraRM);
                    break;
                case "srm_desc":
                    result = result.OrderByDescending(s => s.SifraRM);
                    break;
                case "sjm_asc":
                    result = result.OrderBy(s => s.SifraJM);
                    break;
                case "sjm_desc":
                    result = result.OrderByDescending(s => s.SifraJM);
                    break;
                case "szalhpp_asc":
                    result = result.OrderBy(s => s.StZalihaPocPolugodja);
                    break;
                case "szalhpp_desc":
                    result = result.OrderByDescending(s => s.StZalihaPocPolugodja);
                    break;
                case "szalhkp_asc":
                    result = result.OrderBy(s => s.StZalihaKrPolugodja);
                    break;
                case "szalhkp_desc":
                    result = result.OrderByDescending(s => s.StZalihaKrPolugodja);
                    break;
                default:  // Name ascending 
                    result = result.OrderBy(s => s.SifraVDP);
                    break;
            }

            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(result.ToPagedList(pageNumber, pageSize));
        }

        // GET: StanjeZalihaPP/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: StanjeZalihaPP/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SifraVDP, SifraRM, SifraJM, StZalihaPocPolugodja, StZalihaKrPolugodja")] MasterModel masterModel)
        {
            if (ModelState.IsValid)
            {
                this._stanjeZalihaPPRepository.Create(masterModel);
                return RedirectToAction("Index");
            }
            return View(masterModel);
        }

        // GET: StanjeZalihaPP/Edit/id
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasterModel stanjeZalihaPP = this._stanjeZalihaPPRepository.GetById((int)id);
            if (stanjeZalihaPP == null)
            {
                return HttpNotFound();
            }
            return View(stanjeZalihaPP);
        }

        // POST: StanjeZalihaPP/Edit/id
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MasterModel masterModel)
        {
            if (ModelState.IsValid)
            {
                this._stanjeZalihaPPRepository.Edit(masterModel);
                return RedirectToAction("Index");
            }
            return View(masterModel);
        }

        // GET: StanjeZalihaPP/Delete/id
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasterModel stanjeZalihaPP = this._stanjeZalihaPPRepository.GetById((int)id);
            if (stanjeZalihaPP == null)
            {
                return HttpNotFound();
            }
            return View(stanjeZalihaPP);
        }

        // POST: StanjeZalihaPP/Delete/id
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            this._stanjeZalihaPPRepository.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this._stanjeZalihaPPRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}