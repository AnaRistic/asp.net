﻿using MvcTrgovac.Models;
using MvcTrgovac.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace MvcTrgovac.Controllers
{
    public class StanjeZalihaUController : Controller
    {
        private readonly IStanjeZalihaURepository _stanjeZalihaURepository;

        public StanjeZalihaUController(IStanjeZalihaURepository stanjeZalihaURepository)
        {
            this._stanjeZalihaURepository = stanjeZalihaURepository;
        }

        // GET: StanjeZalihaU
        public ActionResult Index(string sortOrder, string currentFilter, int? page, string searchString, string SifraVDP, string SifraRM, string SifraJM, string StZalihaPocPolugodja, string StZalihaKrPolugodja, string StZalihaPocDrugogPolugodja, string StZalihaKrDrugogPolugodja)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.SvdpSortParm = String.IsNullOrEmpty(sortOrder) ? "svdp_desc" : "";
            ViewBag.SrmSortParm = sortOrder == "srm_asc" ? "srm_desc" : "srm_asc";
            ViewBag.SjmSortParm = sortOrder == "sjm_asc" ? "sjm_desc" : "sjm_asc";
            ViewBag.SzalhPPSortParm = sortOrder == "szalhpp_asc" ? "szalhpp_desc" : "szalhpp_asc";
            ViewBag.SzalhKPSortParm = sortOrder == "szalhkp_asc" ? "szalhkp_desc" : "szalhkp_asc";
            ViewBag.SzalhPDPSortParm = sortOrder == "szalhpdp_asc" ? "szalhpdp_desc" : "szalhpdp_asc";
            ViewBag.SzalhKDPSortParm = sortOrder == "szalhkdp_asc" ? "szalhkdp_desc" : "szalhkdp_asc";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            IQueryable<MasterModel> result = this._stanjeZalihaURepository.GetAll(searchString, SifraVDP, SifraRM, SifraJM, StZalihaPocPolugodja, StZalihaKrPolugodja, StZalihaPocDrugogPolugodja, StZalihaKrDrugogPolugodja);

            switch (sortOrder)
            {
                case "svdp_desc":
                    result = result.OrderByDescending(s => s.SifraVDP);
                    break;
                case "srm_asc":
                    result = result.OrderBy(s => s.SifraRM);
                    break;
                case "srm_desc":
                    result = result.OrderByDescending(s => s.SifraRM);
                    break;
                case "sjm_asc":
                    result = result.OrderBy(s => s.SifraJM);
                    break;
                case "sjm_desc":
                    result = result.OrderByDescending(s => s.SifraJM);
                    break;
                case "szalhpp_asc":
                    result = result.OrderBy(s => s.StZalihaPocPolugodja);
                    break;
                case "szalhpp_desc":
                    result = result.OrderByDescending(s => s.StZalihaPocPolugodja);
                    break;
                case "szalhkp_asc":
                    result = result.OrderBy(s => s.StZalihaKrPolugodja);
                    break;
                case "szalhkp_desc":
                    result = result.OrderByDescending(s => s.StZalihaKrPolugodja);
                    break;
                case "szalhpdp_asc":
                    result = result.OrderBy(s => s.StZalihaPocDrugogPolugodja);
                    break;
                case "szalhpdp_desc":
                    result = result.OrderByDescending(s => s.StZalihaPocDrugogPolugodja);
                    break;
                case "szalhkdp_asc":
                    result = result.OrderBy(s => s.StZalihaKrDrugogPolugodja);
                    break;
                case "szalhkdp_desc":
                    result = result.OrderByDescending(s => s.StZalihaKrDrugogPolugodja);
                    break;
                default:  // Name ascending 
                    result = result.OrderBy(s => s.SifraVDP);
                    break;
            }

            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(result.ToPagedList(pageNumber, pageSize));
        }

        // GET: StanjeZalihaU/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: StanjeZalihaU/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SifraVDP, SifraRM, SifraJM, StZalihaPocPolugodja, StZalihaKrPolugodja, StZalihaPocDrugogPolugodja, StZalihaKrDrugogPolugodja")] MasterModel masterModel)
        {
            if (ModelState.IsValid)
            {
                this._stanjeZalihaURepository.Create(masterModel);
                return RedirectToAction("Index");
            }
            return View(masterModel);
        }

        // GET: StanjeZalihaU/Edit/id
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasterModel stanjeZalihaU = this._stanjeZalihaURepository.GetById((int)id);
            if (stanjeZalihaU == null)
            {
                return HttpNotFound();
            }
            return View(stanjeZalihaU);
        }

        // POST: StanjeZalihaU/Edit/id
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MasterModel masterModel)
        {
            if (ModelState.IsValid)
            {
                this._stanjeZalihaURepository.Edit(masterModel);
                return RedirectToAction("Index");
            }
            return View(masterModel);
        }

        // GET: StanjeZalihaU/Delete/id
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasterModel stanjeZalihaU = this._stanjeZalihaURepository.GetById((int)id);
            if (stanjeZalihaU == null)
            {
                return HttpNotFound();
            }
            return View(stanjeZalihaU);
        }

        // POST: StanjeZalihaU/Delete/id
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            this._stanjeZalihaURepository.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this._stanjeZalihaURepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}