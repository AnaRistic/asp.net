﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcTrgovac.Models;
using MvcTrgovac.Repository;
using MvcTrgovac.Repository.IRepository;

namespace MvcTrgovac.Controllers
{
    public class TrgovacNaMaloesController : Controller
    {
        private readonly ITrgovacNaMaloRepository _trgovacNaMaloRepository;

        public TrgovacNaMaloesController(ITrgovacNaMaloRepository trgovacNaMaloRepository)
        {
            this._trgovacNaMaloRepository = trgovacNaMaloRepository;
        }

        //private Context db = new Context();

        // GET: TrgovacNaMaloes
        public ActionResult Index(string searchString, string BrDozvole, string naziv, string adresa, string PIB, string MaticniBroj)
        {
            List<TrgovacNaMalo> ListTrgovacNaMalo = this._trgovacNaMaloRepository.GetAll(searchString, BrDozvole, naziv, adresa, PIB, MaticniBroj).ToList();
            return View(ListTrgovacNaMalo);
        }
        

        // GET: TrgovacNaMaloes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TrgovacNaMaloes/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,BrDozvole,Naziv,Adresa,PIB,MaticniBroj")] TrgovacNaMalo trgovacNaMalo)
        {
            if (ModelState.IsValid)
            {
                _trgovacNaMaloRepository.Create(trgovacNaMalo);
                return RedirectToAction("Index");
            }

            return View(trgovacNaMalo);
        }

        // GET: TrgovacNaMaloes/Edit/id
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrgovacNaMalo trgovacNaMalo = _trgovacNaMaloRepository.GetById((int)id);
            if (trgovacNaMalo == null)
            {
                return HttpNotFound();
            }
            return View(trgovacNaMalo);
        }

        // POST: TrgovacNaMaloes/Edit/id
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,BrDozvole,Naziv,Adresa,PIB,MaticniBroj")] TrgovacNaMalo trgovacNaMalo)
        {
            if (ModelState.IsValid)
            {
                _trgovacNaMaloRepository.Edit(trgovacNaMalo);
                return RedirectToAction("Index");
            }
            return View(trgovacNaMalo);
        }

        // GET: TrgovacNaMaloes/Delete/id
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrgovacNaMalo trgovacNaMalo = _trgovacNaMaloRepository.GetById((int)id);
            if (trgovacNaMalo == null)
            {
                return HttpNotFound();
            }
            return View(trgovacNaMalo);
        }

        // POST: TrgovacNaMaloes/Delete/id
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _trgovacNaMaloRepository.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _trgovacNaMaloRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
