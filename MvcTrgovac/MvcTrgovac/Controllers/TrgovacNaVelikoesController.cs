﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcTrgovac.Models;
using MvcTrgovac.Repository.IRepository;

namespace MvcTrgovac.Controllers
{
    public class TrgovacNaVelikoesController : Controller
    {
        private readonly ITrgovacNaVelikoRepository _trgovacNaVelikoRepository;

        public TrgovacNaVelikoesController(ITrgovacNaVelikoRepository trgovacNaVelikoRepository)
        {
            this._trgovacNaVelikoRepository = trgovacNaVelikoRepository;
        }
        //private Context db = new Context();

        // GET: TrgovacNaVelikoes
        public ActionResult Index(string searchString, string Datum, string BrojUgovora, string PIB, string naziv)
        {
            List<TrgovacNaVeliko> ListTrgovacNaVeliko = _trgovacNaVelikoRepository.GetAll(searchString, Datum, BrojUgovora, PIB, naziv).ToList();
            return View(ListTrgovacNaVeliko);
        }


        // GET: TrgovacNaVelikoes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TrgovacNaVelikoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Datum,BrojUgovora,PIB,Naziv")] TrgovacNaVeliko trgovacNaVeliko)
        {
            if (ModelState.IsValid)
            {
                _trgovacNaVelikoRepository.Create(trgovacNaVeliko);
                return RedirectToAction("Index");
            }

            return View(trgovacNaVeliko);
        }

        // GET: TrgovacNaVelikoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrgovacNaVeliko trgovacNaVeliko = _trgovacNaVelikoRepository.GetById((int)id);
            if (trgovacNaVeliko == null)
            {
                return HttpNotFound();
            }
            return View(trgovacNaVeliko);
        }

        // POST: TrgovacNaVelikoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Datum,BrojUgovora,PIB,Naziv")] TrgovacNaVeliko trgovacNaVeliko)
        {
            if (ModelState.IsValid)
            {
                _trgovacNaVelikoRepository.Edit(trgovacNaVeliko);
                return RedirectToAction("Index");
            }
            return View(trgovacNaVeliko);
        }

        // GET: TrgovacNaVelikoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrgovacNaVeliko trgovacNaVeliko = _trgovacNaVelikoRepository.GetById((int)id);
            if (trgovacNaVeliko == null)
            {
                return HttpNotFound();
            }
            return View(trgovacNaVeliko);
        }

        // POST: TrgovacNaVelikoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _trgovacNaVelikoRepository.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _trgovacNaVelikoRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
