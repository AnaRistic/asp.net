namespace MvcTrgovac.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MasterModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SifraVDP = c.Int(),
                        SifraRM = c.Int(),
                        SifraJM = c.Int(),
                        KolicinaKupjenihPP = c.Int(),
                        KolicinKupljenihDP = c.Int(),
                        KolicinaKupljenihU = c.Int(),
                        Tip = c.String(),
                        KolicinaProdatihPP = c.Int(),
                        KolicinaProdatihDP = c.Int(),
                        KolicinaProdatihU = c.Int(),
                        StZalihaPocDrugogPolugodja = c.Int(),
                        StZalihaKrDrugogPolugodja = c.Int(),
                        StZalihaPocPolugodja = c.Int(),
                        StZalihaKrPolugodja = c.Int(),
                        Status = c.Int(nullable: false),
                        DeleteStatus = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TrgovacNaMaloes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        BrDozvole = c.Int(nullable: false),
                        Naziv = c.String(nullable: false),
                        Adresa = c.String(),
                        PIB = c.Int(nullable: false),
                        MaticniBroj = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TrgovacNaVelikoes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Datum = c.DateTime(nullable: false),
                        BrojUgovora = c.Int(nullable: false),
                        PIB = c.Int(nullable: false),
                        Naziv = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TrgovacNaVelikoes");
            DropTable("dbo.TrgovacNaMaloes");
            DropTable("dbo.MasterModels");
        }
    }
}
