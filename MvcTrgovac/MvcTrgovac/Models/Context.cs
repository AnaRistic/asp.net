﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MvcTrgovac.Models
{
    public class Context : DbContext
    {
        public DbSet<TrgovacNaMalo> TrgovacNaMalo { get; set; }
        public DbSet<TrgovacNaVeliko> TrgovacNaVeliko { get; set; }
        public DbSet<MasterModel> MasterModel { get; set; }
    }
}