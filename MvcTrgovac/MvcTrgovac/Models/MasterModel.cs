﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcTrgovac.Models
{
    public class MasterModel
    {
        public int Id { get; set; }
        [Display(Name = "Šifra vrste duvanskog proizvoda")]
        public Nullable<int> SifraVDP { get; set; } //vrsta duvanskog proizvoda
        [Display(Name = "Šifra robne marke")]
        public Nullable<int> SifraRM { get; set; } // robne marke
        [Display(Name = "Šifra jedinice mere")]
        public Nullable<int> SifraJM { get; set; }  // jedinice mere
        [Display(Name = "Količina kupljenih 1.1-30.6")]
        public Nullable<int> KolicinaKupjenihPP { get; set; } // prvo polugodiste
        [Display(Name = "Količina kupljenih 1.7-30.12")]
        public Nullable<int> KolicinKupljenihDP { get; set; } // drugo polugodiste
        [Display(Name = "Količina kupljenih ukupno")]
        public Nullable<int> KolicinaKupljenihU { get; set; } // ukupno
        public string Tip { get; set; }
        [Display(Name = "Količina prodatih 1.1-30.6")]
        public Nullable<int> KolicinaProdatihPP { get; set; }
        [Display(Name = "Količina prodatih 1.7-30.12")]
        public Nullable<int> KolicinaProdatihDP { get; set; }
        [Display(Name = "Količina prodatih ukupno")]
        public Nullable<int> KolicinaProdatihU { get; set; }
        [Display(Name = "Stanje zaliha 1.7")]
        public Nullable<int> StZalihaPocDrugogPolugodja { get; set; }
        [Display(Name = "Stanje zaliha 31.12")]
        public Nullable<int> StZalihaKrDrugogPolugodja { get; set; }
        [Display(Name = "Stanje zaliha 1.1")]
        public Nullable<int> StZalihaPocPolugodja { get; set; }
        [Display(Name = "Stanje zaliha 30.6")]
        public Nullable<int> StZalihaKrPolugodja { get; set; }
        public int Status { get; set; }
        public bool DeleteStatus { get; set; }
    }
}