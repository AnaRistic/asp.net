﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MvcTrgovac.Models
{
    public class TrgovacNaMalo
    {
        public int ID { get; set; }
        [Display(Name = "Broj Dozvole")]
        [Required]
        [Range(1, 1000)]
        public int BrDozvole { get; set; }
        [Required]
        public string Naziv { get; set; }
        public string Adresa { get; set; }
        [Required]
        [Range(1, 100000000)]
        public int PIB { get; set; }
        [Display(Name = "Matični broj")]
        [Required]
        [Range(1, 10000000000)]
        public int MaticniBroj { get; set; }
    }
    
}