﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcTrgovac.Models
{
    public class TrgovacNaVeliko
    {
        public int ID { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Datum { get; set; }
        [Display(Name = "Broj Ugovora")]
        [Required]
        [Range(1, 1000)]
        public int BrojUgovora { get; set; }
        [Required]
        [Range(1, 1000000000)]
        public int PIB { get; set; }
        [Required]
        public string Naziv { get; set; }
    }
}