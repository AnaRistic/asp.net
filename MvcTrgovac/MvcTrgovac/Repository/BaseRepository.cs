﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcTrgovac.Models;

namespace MvcTrgovac.Repository
{
    public class BaseRepository
    {
        protected readonly Context context;

        public BaseRepository(Context context)
        {
            this.context = context;
        }

        public void SaveChanges()
        {
            this.context.SaveChanges();
        }
    }
}