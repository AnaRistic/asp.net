﻿using MvcTrgovac.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvcTrgovac.Repository.IRepository
{
    public interface IKupljeniProizvodiPPRepository
    {
        IQueryable<MasterModel> GetAll(string searchString, string SifraVDP, string SifraRM, string SifraJM, string KolicinaProdatihPP);
        MasterModel GetById(int id);
        void Create(MasterModel masterModel);
        void Edit(MasterModel masterModel);
        void Delete(int id);
        void Dispose();
    }
}
