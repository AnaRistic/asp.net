﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvcTrgovac.Models;

namespace MvcTrgovac.Repository.IRepository
{
    public interface ITrgovacNaMaloRepository
    {
        IQueryable<TrgovacNaMalo> GetAll(string searchString, string BrDozvole, string naziv, string adresa, string PIB, string MaticniBroj);
        void Create(TrgovacNaMalo TrgovacNaMalo);
        void Edit(TrgovacNaMalo TrgovacNaMalo);
        void Delete(int id);
        TrgovacNaMalo GetById(int id);
        void Dispose();
    }
}
