﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvcTrgovac.Models;

namespace MvcTrgovac.Repository.IRepository
{
    public interface ITrgovacNaVelikoRepository
    {
        IQueryable<TrgovacNaVeliko> GetAll(string searchString, string Datum, string BrojUgovora, string PIB, string naziv);
        void Create(TrgovacNaVeliko TrgovacNaVeliko);
        void Edit(TrgovacNaVeliko TrgovacNaVeliko);
        void Delete(int id);
        TrgovacNaVeliko GetById(int id);
        void Dispose();
    }
}
