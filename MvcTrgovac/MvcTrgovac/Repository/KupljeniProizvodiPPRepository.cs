﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MvcTrgovac.Models;
using MvcTrgovac.Repository.IRepository;


namespace MvcTrgovac.Repository
{
    public class KupljeniProizvodiPPRepository : BaseRepository, IKupljeniProizvodiPPRepository
    {
        public KupljeniProizvodiPPRepository(Context context) : base(context)
        {

        }

        public IQueryable<MasterModel> GetAll(string searchString, string SifraVDP, string SifraRM, string SifraJM, string KolicinaKupjenihPP)
        {
            int parsedSearch;
            int.TryParse(searchString, out parsedSearch);

            int parsedSifraVDP;
            int.TryParse(SifraVDP, out parsedSifraVDP);

            int parsedSifraRM;
            int.TryParse(SifraRM, out parsedSifraRM);

            int parsedSifraJM;
            int.TryParse(SifraJM, out parsedSifraJM);

            int parsedKolicinaKupjenihPP;
            int.TryParse(KolicinaKupjenihPP, out parsedKolicinaKupjenihPP);

            var query = from kp in context.MasterModel
                        where kp.Status == 1 && kp.DeleteStatus == false
                        select kp;

            if (!String.IsNullOrEmpty(searchString))
            {
                query = query.Where(kp => kp.SifraVDP == parsedSearch ||
                                          kp.SifraRM == parsedSearch ||
                                          kp.SifraJM == parsedSearch ||
                                          kp.KolicinaKupjenihPP == parsedSearch);
            }
            if(parsedSifraVDP != 0)
            {
                query = query.Where(kp => kp.SifraVDP == parsedSifraVDP);
            }
            if (parsedSifraRM != 0)
            {
                query = query.Where(kp => kp.SifraRM == parsedSifraRM);
            }
            if (parsedSifraJM != 0)
            {
                query = query.Where(kp => kp.SifraJM == parsedSifraJM);
            }
            if (parsedKolicinaKupjenihPP != 0)
            {
                query = query.Where(kp => kp.KolicinaKupjenihPP == parsedKolicinaKupjenihPP);
            }

            return query;
        }

        public MasterModel GetById(int id)
        {
            MasterModel kupljeniPoizPP = context.MasterModel.Find(id);
            return kupljeniPoizPP;
        }

        public void Create(MasterModel masterModel)
        {
            masterModel.DeleteStatus = false;
            masterModel.Status = 1;
            context.MasterModel.Add(masterModel);
            base.SaveChanges();
        }

        public void Edit(MasterModel masterModel)
        {
            int id = masterModel.Id;
            MasterModel kupljeniProizPP = this.GetById(id);
            kupljeniProizPP.SifraVDP = masterModel.SifraVDP;
            kupljeniProizPP.SifraRM = masterModel.SifraRM;
            kupljeniProizPP.SifraJM = masterModel.SifraJM;
            kupljeniProizPP.KolicinaKupjenihPP = masterModel.KolicinaKupjenihPP;
            base.SaveChanges();
        }

        public void Delete(int id)
        {
            MasterModel kupljeniProizPP = this.GetById(id);
            kupljeniProizPP.DeleteStatus = true;
            //context.MasterModel.Remove(kupljeniProizPP);
            base.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}