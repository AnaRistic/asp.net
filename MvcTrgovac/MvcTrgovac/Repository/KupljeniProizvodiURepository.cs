﻿using MvcTrgovac.Models;
using MvcTrgovac.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcTrgovac.Repository
{
    public class KupljeniProizvodiURepository : BaseRepository, IKupljeniProizvodiURepository
    {
        public KupljeniProizvodiURepository(Context context) : base(context)
        {

        }

        public IQueryable<MasterModel> GetAll(string searchString, string SifraVDP, string SifraRM, string SifraJM, string KolicinaKupjenihPP, string KolicinKupljenihDP, string KolicinaKupljenihU)
        {
            int parsedSearch;
            int.TryParse(searchString, out parsedSearch);

            int parsedSifraVDP;
            int.TryParse(SifraVDP, out parsedSifraVDP);

            int parsedSifraRM;
            int.TryParse(SifraRM, out parsedSifraRM);

            int parsedSifraJM;
            int.TryParse(SifraJM, out parsedSifraJM);

            int parsedKolicinaKupjenihPP;
            int.TryParse(KolicinaKupjenihPP, out parsedKolicinaKupjenihPP);

            int parsedKolicinKupljenihDP;
            int.TryParse(KolicinKupljenihDP, out parsedKolicinKupljenihDP);

            int parsedKolicinaKupljenihU;
            int.TryParse(KolicinaKupljenihU, out parsedKolicinaKupljenihU);

            var query = from kp in context.MasterModel
                        where kp.Status == 1 && kp.DeleteStatus == false
                        select kp;

            if (!String.IsNullOrEmpty(searchString))
            {
                query = query.Where(kp => kp.SifraVDP == parsedSearch ||
                                          kp.SifraRM == parsedSearch ||
                                          kp.SifraJM == parsedSearch ||
                                          kp.KolicinaKupjenihPP == parsedSearch ||
                                          kp.KolicinKupljenihDP == parsedSearch ||
                                          kp.KolicinaKupljenihU == parsedSearch);
            }
            if (parsedSifraVDP != 0)
            {
                query = query.Where(kp => kp.SifraVDP == parsedSifraVDP);
            }
            if (parsedSifraRM != 0)
            {
                query = query.Where(kp => kp.SifraRM == parsedSifraRM);
            }
            if (parsedSifraJM != 0)
            {
                query = query.Where(kp => kp.SifraJM == parsedSifraJM);
            }
            if (parsedKolicinaKupjenihPP != 0)
            {
                query = query.Where(kp => kp.KolicinaKupjenihPP == parsedKolicinaKupjenihPP);
            }
            if (parsedKolicinKupljenihDP != 0)
            {
                query = query.Where(kp => kp.KolicinKupljenihDP == parsedKolicinKupljenihDP);
            }
            if (parsedKolicinaKupljenihU != 0)
            {
                query = query.Where(kp => kp.KolicinaKupljenihU == parsedKolicinaKupljenihU);
            }

            return query;
        }

        public MasterModel GetById(int id)
        {
            MasterModel kupljeniPoizU = context.MasterModel.Find(id);
            return kupljeniPoizU;
        }

        public void Create(MasterModel masterModel)
        {
            masterModel.DeleteStatus = false;
            masterModel.Status = 1;
            masterModel.KolicinaKupljenihU = masterModel.KolicinaKupjenihPP + masterModel.KolicinKupljenihDP;

            context.MasterModel.Add(masterModel);
            base.SaveChanges();
        }

        public void Edit(MasterModel masterModel)
        {
            int id = masterModel.Id;
            MasterModel kupljeniProizU = this.GetById(id);
            kupljeniProizU.SifraVDP = masterModel.SifraVDP;
            kupljeniProizU.SifraRM = masterModel.SifraRM;
            kupljeniProizU.SifraJM = masterModel.SifraJM;
            kupljeniProizU.KolicinaKupjenihPP = masterModel.KolicinaKupjenihPP;
            kupljeniProizU.KolicinKupljenihDP = masterModel.KolicinKupljenihDP;
            kupljeniProizU.KolicinaKupljenihU = masterModel.KolicinaKupjenihPP + masterModel.KolicinKupljenihDP;
            base.SaveChanges();
        }

        public void Delete(int id)
        {
            MasterModel kupljeniProizU = this.GetById(id);
            kupljeniProizU.DeleteStatus = true;
            //context.MasterModel.Remove(kupljeniProizU);
            base.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}