﻿using MvcTrgovac.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcTrgovac.Models;

namespace MvcTrgovac.Repository
{
    public class ProdatiProizvodiPPRepository : BaseRepository, IProdatiProizvodiPPRepository
    {
        public ProdatiProizvodiPPRepository(Context context) : base(context)
        {

        }

        public IQueryable<MasterModel> GetAll(string searchString, string SifraVDP, string SifraRM, string SifraJM, string KolicinaProdatihPP)
        {
            int parsedSearch;
            int.TryParse(searchString, out parsedSearch);

            int parsedSifraVDP;
            int.TryParse(SifraVDP, out parsedSifraVDP);

            int parsedSifraRM;
            int.TryParse(SifraRM, out parsedSifraRM);

            int parsedSifraJM;
            int.TryParse(SifraJM, out parsedSifraJM);

            int parsedKolicinaProdatihPP;
            int.TryParse(KolicinaProdatihPP, out parsedKolicinaProdatihPP);

            var query = from kp in context.MasterModel
                        where kp.Status == 2 && kp.DeleteStatus == false
                        select kp;

            if (!String.IsNullOrEmpty(searchString))
            {
                query = query.Where(kp => kp.SifraVDP == parsedSearch ||
                                          kp.SifraRM == parsedSearch ||
                                          kp.SifraJM == parsedSearch ||
                                          kp.KolicinaProdatihPP == parsedSearch);
            }
            if (parsedSifraVDP != 0)
            {
                query = query.Where(kp => kp.SifraVDP == parsedSifraVDP);
            }
            if (parsedSifraRM != 0)
            {
                query = query.Where(kp => kp.SifraRM == parsedSifraRM);
            }
            if (parsedSifraJM != 0)
            {
                query = query.Where(kp => kp.SifraJM == parsedSifraJM);
            }
            if (parsedKolicinaProdatihPP != 0)
            {
                query = query.Where(kp => kp.KolicinaProdatihPP == parsedKolicinaProdatihPP);
            }

            return query;
        }

        public MasterModel GetById(int id)
        {
            MasterModel prodatiPoizPP = context.MasterModel.Find(id);
            return prodatiPoizPP;
        }

        public void Create(MasterModel masterModel)
        {
            masterModel.Status = 2;
            masterModel.DeleteStatus = false;
            context.MasterModel.Add(masterModel);
            base.SaveChanges();
        }

        public void Edit(MasterModel masterModel)
        {
            int id = masterModel.Id;
            MasterModel prodatiProizPP = this.GetById(id);
            prodatiProizPP.SifraVDP = masterModel.SifraVDP;
            prodatiProizPP.SifraRM = masterModel.SifraRM;
            prodatiProizPP.SifraJM = masterModel.SifraJM;
            prodatiProizPP.KolicinaProdatihPP = masterModel.KolicinaProdatihPP;
            base.SaveChanges();
        }

        public void Delete(int id)
        {
            MasterModel prodatiProizPP = this.GetById(id);
            prodatiProizPP.DeleteStatus = true;
            //context.MasterModel.Remove(prodatiProizPP);
            base.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}