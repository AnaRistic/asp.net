﻿using MvcTrgovac.Models;
using MvcTrgovac.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcTrgovac.Repository
{
    public class ProdatiProizvodiURepository : BaseRepository, IProdatiProizvodiURepository
    {
        public ProdatiProizvodiURepository(Context context) : base(context)
        {

        }

        public IQueryable<MasterModel> GetAll(string searchString, string SifraVDP, string SifraRM, string SifraJM, string KolicinaProdatihPP, string KolicinaProdatihDP, string KolicinaProdatihU)
        {
            int parsedSearch;
            int.TryParse(searchString, out parsedSearch);

            int parsedSifraVDP;
            int.TryParse(SifraVDP, out parsedSifraVDP);

            int parsedSifraRM;
            int.TryParse(SifraRM, out parsedSifraRM);

            int parsedSifraJM;
            int.TryParse(SifraJM, out parsedSifraJM);

            int parsedKolicinaProdatihPP;
            int.TryParse(KolicinaProdatihPP, out parsedKolicinaProdatihPP);

            int parsedKolicinaProdatihDP;
            int.TryParse(KolicinaProdatihDP, out parsedKolicinaProdatihDP);

            int parsedKolicinaProdatihU;
            int.TryParse(KolicinaProdatihU, out parsedKolicinaProdatihU);

            var query = from kp in context.MasterModel
                        where kp.Status == 2 && kp.DeleteStatus == false
                        select kp;

            if (!String.IsNullOrEmpty(searchString))
            {
                query = query.Where(kp => kp.SifraVDP == parsedSearch ||
                                          kp.SifraRM == parsedSearch ||
                                          kp.SifraJM == parsedSearch ||
                                          kp.KolicinaKupjenihPP == parsedSearch ||
                                          kp.KolicinaProdatihDP == parsedSearch ||
                                          kp.KolicinaProdatihU == parsedSearch);
            }
            if (parsedSifraVDP != 0)
            {
                query = query.Where(kp => kp.SifraVDP == parsedSifraVDP);
            }
            if (parsedSifraRM != 0)
            {
                query = query.Where(kp => kp.SifraRM == parsedSifraRM);
            }
            if (parsedSifraJM != 0)
            {
                query = query.Where(kp => kp.SifraJM == parsedSifraJM);
            }
            if (parsedKolicinaProdatihDP != 0)
            {
                query = query.Where(kp => kp.KolicinaProdatihDP == parsedKolicinaProdatihDP);
            }
            if (parsedKolicinaProdatihU != 0)
            {
                query = query.Where(kp => kp.KolicinaProdatihU == parsedKolicinaProdatihU);
            }

            return query;
        }

        public MasterModel GetById(int id)
        {
            MasterModel prodatiPoizU = context.MasterModel.Find(id);
            return prodatiPoizU;
        }

        public void Create(MasterModel masterModel)
        {
            masterModel.DeleteStatus = false;
            masterModel.Status = 2;
            masterModel.KolicinaProdatihU = masterModel.KolicinaProdatihPP + masterModel.KolicinaProdatihDP;
            context.MasterModel.Add(masterModel);
            base.SaveChanges();
        }

        public void Edit(MasterModel masterModel)
        {
            int id = masterModel.Id;
            MasterModel prodatiProizU = this.GetById(id);
            prodatiProizU.SifraVDP = masterModel.SifraVDP;
            prodatiProizU.SifraRM = masterModel.SifraRM;
            prodatiProizU.SifraJM = masterModel.SifraJM;
            prodatiProizU.KolicinaProdatihPP = masterModel.KolicinaProdatihPP;
            prodatiProizU.KolicinaProdatihDP = masterModel.KolicinaProdatihDP;
            prodatiProizU.KolicinaProdatihU = masterModel.KolicinaProdatihPP + masterModel.KolicinaProdatihDP;
            base.SaveChanges();
        }

        public void Delete(int id)
        {
            MasterModel prodatiProizU = this.GetById(id);
            prodatiProizU.DeleteStatus = true;
            //context.MasterModel.Remove(prodatiProizU);
            base.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}