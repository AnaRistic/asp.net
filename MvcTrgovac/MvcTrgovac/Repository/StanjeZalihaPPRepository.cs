﻿using MvcTrgovac.Repository.IRepository;
using MvcTrgovac.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcTrgovac.Repository
{
    public class StanjeZalihaPPRepository : BaseRepository, IStanjeZalihaPPRepository
    {
        public StanjeZalihaPPRepository(Context context) : base(context)
        {

        }

        public IQueryable<MasterModel> GetAll(string searchString, string SifraVDP, string SifraRM, string SifraJM, string StZalihaPocPolugodja, string StZalihaKrPolugodja)
        {
            int parsedSearch;
            int.TryParse(searchString, out parsedSearch);

            int parsedSifraVDP;
            int.TryParse(SifraVDP, out parsedSifraVDP);

            int parsedSifraRM;
            int.TryParse(SifraRM, out parsedSifraRM);

            int parsedSifraJM;
            int.TryParse(SifraJM, out parsedSifraJM);

            int parsedStZalihaPocPolugodja;
            int.TryParse(StZalihaPocPolugodja, out parsedStZalihaPocPolugodja);

            int parsedStZalihaKrPolugodja;
            int.TryParse(StZalihaKrPolugodja, out parsedStZalihaKrPolugodja);

            var query = from kp in context.MasterModel
                        where kp.Status == 3 && kp.DeleteStatus == false
                        select kp;

            if (!String.IsNullOrEmpty(searchString))
            {
                query = query.Where(kp => kp.SifraVDP == parsedSearch ||
                                          kp.SifraRM == parsedSearch ||
                                          kp.SifraJM == parsedSearch ||
                                          kp.StZalihaPocPolugodja == parsedSearch ||
                                          kp.StZalihaKrPolugodja == parsedSearch );
            }
            if (parsedSifraVDP != 0)
            {
                query = query.Where(kp => kp.SifraVDP == parsedSifraVDP);
            }
            if (parsedSifraRM != 0)
            {
                query = query.Where(kp => kp.SifraRM == parsedSifraRM);
            }
            if (parsedSifraJM != 0)
            {
                query = query.Where(kp => kp.SifraJM == parsedSifraJM);
            }
            if (parsedStZalihaPocPolugodja != 0)
            {
                query = query.Where(kp => kp.StZalihaPocPolugodja == parsedStZalihaPocPolugodja);
            }
            if (parsedStZalihaKrPolugodja != 0)
            {
                query = query.Where(kp => kp.StZalihaKrPolugodja == parsedStZalihaKrPolugodja);
            }

            return query;
        }

        public MasterModel GetById(int id)
        {
            MasterModel stanjeZalihaPP = context.MasterModel.Find(id);
            return stanjeZalihaPP;
        }

        public void Create(MasterModel masterModel)
        {
            masterModel.Status = 3;
            masterModel.DeleteStatus = false;
            context.MasterModel.Add(masterModel);
            base.SaveChanges();
        }

        public void Edit(MasterModel masterModel)
        {
            int id = masterModel.Id;
            MasterModel stanjeZalihaPP = this.GetById(id);
            stanjeZalihaPP.SifraVDP = masterModel.SifraVDP;
            stanjeZalihaPP.SifraRM = masterModel.SifraRM;
            stanjeZalihaPP.SifraJM = masterModel.SifraJM;
            stanjeZalihaPP.StZalihaPocPolugodja = masterModel.StZalihaPocPolugodja;
            stanjeZalihaPP.StZalihaKrPolugodja = masterModel.StZalihaKrPolugodja;
            base.SaveChanges();
        }

        public void Delete(int id)
        {
            MasterModel stanjeZalihaPP = this.GetById(id);
            stanjeZalihaPP.DeleteStatus = true;
            //context.MasterModel.Remove(stanjeZalihaPP);
            base.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}