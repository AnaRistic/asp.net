﻿using MvcTrgovac.Models;
using MvcTrgovac.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcTrgovac.Repository
{
    public class StanjeZalihaURepository : BaseRepository, IStanjeZalihaURepository
    {
        public StanjeZalihaURepository(Context context) : base(context)
        {

        }

        public IQueryable<MasterModel> GetAll(string searchString, string SifraVDP, string SifraRM, string SifraJM, string StZalihaPocPolugodja, string StZalihaKrPolugodja, string StZalihaPocDrugogPolugodja, string StZalihaKrDrugogPolugodja)
        {
            int parsedSearch;
            int.TryParse(searchString, out parsedSearch);

            int parsedSifraVDP;
            int.TryParse(SifraVDP, out parsedSifraVDP);

            int parsedSifraRM;
            int.TryParse(SifraRM, out parsedSifraRM);

            int parsedSifraJM;
            int.TryParse(SifraJM, out parsedSifraJM);

            int parsedStZalihaPocPolugodja;
            int.TryParse(StZalihaPocPolugodja, out parsedStZalihaPocPolugodja);

            int parsedStZalihaKrPolugodja;
            int.TryParse(StZalihaKrPolugodja, out parsedStZalihaKrPolugodja);

            int parsedStZalihaPocDrugogPolugodja;
            int.TryParse(StZalihaPocDrugogPolugodja, out parsedStZalihaPocDrugogPolugodja);

            int parsedStZalihaKrDrugogPolugodja;
            int.TryParse(StZalihaKrDrugogPolugodja, out parsedStZalihaKrDrugogPolugodja);

            var query = from kp in context.MasterModel
                        where kp.Status == 3 && kp.DeleteStatus == false
                        select kp;

            if (!String.IsNullOrEmpty(searchString))
            {
                query = query.Where(kp => kp.SifraVDP == parsedSearch ||
                                          kp.SifraRM == parsedSearch ||
                                          kp.SifraJM == parsedSearch ||
                                          kp.StZalihaPocPolugodja == parsedSearch ||
                                          kp.StZalihaKrPolugodja == parsedSearch ||
                                          kp.StZalihaPocDrugogPolugodja == parsedSearch ||
                                          kp.StZalihaKrDrugogPolugodja == parsedSearch);
            }
            if (parsedSifraVDP != 0)
            {
                query = query.Where(kp => kp.SifraVDP == parsedSifraVDP);
            }
            if (parsedSifraRM != 0)
            {
                query = query.Where(kp => kp.SifraRM == parsedSifraRM);
            }
            if (parsedSifraJM != 0)
            {
                query = query.Where(kp => kp.SifraJM == parsedSifraJM);
            }
            if (parsedStZalihaPocPolugodja != 0)
            {
                query = query.Where(kp => kp.StZalihaPocPolugodja == parsedStZalihaPocPolugodja);
            }
            if (parsedStZalihaKrPolugodja != 0)
            {
                query = query.Where(kp => kp.StZalihaKrPolugodja == parsedStZalihaKrPolugodja);
            }
            if (parsedStZalihaPocDrugogPolugodja != 0)
            {
                query = query.Where(kp => kp.StZalihaPocDrugogPolugodja == parsedStZalihaPocDrugogPolugodja);
            }
            if (parsedStZalihaKrDrugogPolugodja != 0)
            {
                query = query.Where(kp => kp.StZalihaKrDrugogPolugodja == parsedStZalihaKrDrugogPolugodja);
            }

            return query;
        }

        public MasterModel GetById(int id)
        {
            MasterModel stanjeZalihaU = context.MasterModel.Find(id);
            return stanjeZalihaU;
        }

        public void Create(MasterModel masterModel)
        {
            masterModel.Status = 3;
            masterModel.DeleteStatus = false;
            context.MasterModel.Add(masterModel);
            base.SaveChanges();
        }

        public void Edit(MasterModel masterModel)
        {
            int id = masterModel.Id;
            MasterModel stanjeZalihaU = this.GetById(id);
            stanjeZalihaU.SifraVDP = masterModel.SifraVDP;
            stanjeZalihaU.SifraRM = masterModel.SifraRM;
            stanjeZalihaU.SifraJM = masterModel.SifraJM;
            stanjeZalihaU.StZalihaPocPolugodja = masterModel.StZalihaPocPolugodja;
            stanjeZalihaU.StZalihaKrPolugodja = masterModel.StZalihaKrPolugodja;
            stanjeZalihaU.StZalihaPocDrugogPolugodja = masterModel.StZalihaPocDrugogPolugodja;
            stanjeZalihaU.StZalihaKrDrugogPolugodja = masterModel.StZalihaKrDrugogPolugodja;
            base.SaveChanges();
        }

        public void Delete(int id)
        {
            MasterModel stanjeZalihaU = this.GetById(id);
            stanjeZalihaU.DeleteStatus = true;
            //context.MasterModel.Remove(stanjeZalihaU);
            base.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}