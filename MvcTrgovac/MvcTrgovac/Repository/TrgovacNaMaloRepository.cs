﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MvcTrgovac.Models;
using MvcTrgovac.Repository.IRepository;

namespace MvcTrgovac.Repository
{
    public class TrgovacNaMaloRepository : BaseRepository, ITrgovacNaMaloRepository
    {
        public TrgovacNaMaloRepository(Context context) : base(context)
        {

        }

        public void Create(TrgovacNaMalo TrgovacNaMalo)
        {
            context.TrgovacNaMalo.Add(TrgovacNaMalo);
            base.SaveChanges();
        }

        public void Delete(int id)
        {
            TrgovacNaMalo trgovacNaMalo = this.GetById(id);
            context.TrgovacNaMalo.Remove(trgovacNaMalo);
            base.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public void Edit(TrgovacNaMalo TrgovacNaMalo)
        {
            context.Entry(TrgovacNaMalo).State = EntityState.Modified;
            base.SaveChanges();
        }

        public IQueryable<TrgovacNaMalo> GetAll(string searchString, string BrDozvole, string naziv, string adresa, string PIB, string MaticniBroj)
        {
            int parsedSearch;
            int.TryParse(searchString, out parsedSearch);

            int parsedBrDozvole;
            int.TryParse(BrDozvole, out parsedBrDozvole);

            int parsedPIB;
            int.TryParse(PIB, out parsedPIB);

            int parsedMaticniBr;
            int.TryParse(MaticniBroj, out parsedMaticniBr);

            var query = from tnm in context.TrgovacNaMalo
                        select tnm;
            if (!String.IsNullOrEmpty(searchString))
            {
                query = query.Where(n => n.BrDozvole == parsedSearch ||
                                         n.Naziv.Contains(searchString) ||
                                         n.Adresa.Contains(searchString) ||
                                         n.PIB == parsedSearch ||
                                         n.MaticniBroj == parsedSearch);
            }
            if (parsedBrDozvole != 0)
            {
                query = query.Where(n => n.BrDozvole == parsedBrDozvole);
            }
            if (!String.IsNullOrEmpty(naziv))
            {
                query = query.Where(n => n.Naziv.Contains(naziv));
            }
            if (!String.IsNullOrEmpty(adresa))
            {
                query = query.Where(n => n.Adresa.Contains(adresa));
            }
            if (parsedPIB != 0)
            {
                query = query.Where(n => n.PIB == parsedPIB);
            }
            if (parsedMaticniBr != 0)
            {
                query = query.Where(n => n.MaticniBroj == parsedMaticniBr);
            }
            return query;
        }

        public TrgovacNaMalo GetById(int id)
        {
            TrgovacNaMalo trgovacNaMalo = context.TrgovacNaMalo.Find(id);
            return trgovacNaMalo;
        }
    }
}