﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MvcTrgovac.Models;
using MvcTrgovac.Repository.IRepository;

namespace MvcTrgovac.Repository
{
    public class TrgovacNaVelikoRepository : BaseRepository, ITrgovacNaVelikoRepository
    {
        public TrgovacNaVelikoRepository(Context context) : base(context)
        {

        }

        public void Create(TrgovacNaVeliko TrgovacNaVeliko)
        {
            context.TrgovacNaVeliko.Add(TrgovacNaVeliko);
            base.SaveChanges();
        }

        public void Delete(int id)
        {
            TrgovacNaVeliko trgovacNaVeliko = this.GetById(id);
            context.TrgovacNaVeliko.Remove(trgovacNaVeliko);
            base.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public void Edit(TrgovacNaVeliko TrgovacNaVeliko)
        {
            context.Entry(TrgovacNaVeliko).State = EntityState.Modified;
            base.SaveChanges();
        }

        /*
         * Editovanje na drugi nacin
         * 
        public void EditTrgovacNovo(int id)
        {
            TrgovacNaVeliko trgovac = context.TrgovacNaVeliko.FirstOrDefault(x => x.ID == id);
            trgovac.Naziv = "Primer";
            context.SaveChanges();
        }
        */

        public IQueryable<TrgovacNaVeliko> GetAll(string searchString, string Datum, string BrojUgovora, string PIB, string naziv)
        {
            // Parsovanje
            int parsedSearch;
            int.TryParse(searchString, out parsedSearch);

            DateTime Date;
            DateTime.TryParse(Datum, out Date);

            int parsedBrUgovora;
            int.TryParse(BrojUgovora, out parsedBrUgovora);

            int parsedPIB;
            int.TryParse(PIB, out parsedPIB);

            DateTime SearchDate;
            DateTime.TryParse(searchString, out SearchDate);

            // Data Logika
            var query = from tnv in context.TrgovacNaVeliko
                        select tnv;

            // Business logika
            if (!String.IsNullOrEmpty(searchString))
            {
                query = query.Where(s => s.Datum == SearchDate ||
                                         s.BrojUgovora == parsedSearch ||
                                         s.PIB == parsedSearch ||
                                         s.Naziv.Contains(searchString));
            }
            if (Datum != null)
            {
                query = query.Where(s => s.Datum == Date);
            }
            if (parsedBrUgovora != 0)
            {
                query = query.Where(s => s.BrojUgovora == parsedBrUgovora);
            }
            if (parsedPIB != 0)
            {
                query = query.Where(s => s.PIB == parsedPIB);
            }
            if (!String.IsNullOrEmpty(naziv))
            {
                query = query.Where(s => s.Naziv.Contains(naziv));
            }

            return query;
        }

        public TrgovacNaVeliko GetById(int id)
        {
            TrgovacNaVeliko trgovacNaVeliko = context.TrgovacNaVeliko.Find(id);
            return trgovacNaVeliko;
        }
    }
}