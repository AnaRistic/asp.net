﻿using Prekvalifikacija.Models;
using Prekvalifikacija.Rapository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Prekvalifikacija.Controllers
{
    public class StudentsController : ApiController
    {
        private readonly IStudentRepository _studentRepository;

        public StudentsController(IStudentRepository studentRepository)
        {
            this._studentRepository = studentRepository;
        }

        // GET: api/students
        public List<Student> Get()
        {
            return this._studentRepository.Get();
        }

        // GET: api/student/id
        [HttpGet, Route("api/student/{id}")]
        public HttpResponseMessage Get(int id)
        {
            var student = this._studentRepository.GetByID(id);

            if (student != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, student);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Student sa id = " + id + " ne postoji");
            }
        }

        // POST: api/student
        [HttpPost, Route("api/student")]
        public HttpResponseMessage Post(Student student)
        {
            try
            {
                this._studentRepository.Post(student);

                var message = Request.CreateResponse(HttpStatusCode.Created, student);
                message.Headers.Location = new Uri(Request.RequestUri + student.Id.ToString());
                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // PUT: api/student/id
        [HttpPut, Route("api/student/{id}")]
        public HttpResponseMessage Put(int id, Student student)
        {
            try
            {
                if (this._studentRepository.Put(id, student) != null)
                {
                    Student result = this._studentRepository.Put(id, student);
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Student sa id = " + id + " ne postoji");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        // DELETE: api/student/id
        [HttpDelete, Route("api/student/{id}")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                if (this._studentRepository.Delete(id) == true)
                {
                    this._studentRepository.Delete(id);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Student sa id = " + id + " ne postoji");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
