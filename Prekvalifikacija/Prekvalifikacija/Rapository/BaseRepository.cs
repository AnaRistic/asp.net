﻿using Prekvalifikacija.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Prekvalifikacija.Rapository
{
    public class BaseRepository
    {
        protected readonly PrekvalifikacijaDBEntities context;

        public BaseRepository(PrekvalifikacijaDBEntities context)
        {
            this.context = context;
        }
    }
}