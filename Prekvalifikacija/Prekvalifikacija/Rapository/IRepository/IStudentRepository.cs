﻿using Prekvalifikacija.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prekvalifikacija.Rapository.IRepository
{
    public interface IStudentRepository
    {
        List<Student> Get();
        Student GetByID(int id);
        void Post(Student student);
        Student Put(int id, Student student);
        bool Delete(int id);
    }
}
