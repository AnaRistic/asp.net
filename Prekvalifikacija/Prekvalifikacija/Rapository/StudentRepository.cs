﻿using Prekvalifikacija.Models;
using Prekvalifikacija.Rapository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Prekvalifikacija.Rapository
{
    public class StudentRepository : BaseRepository, IStudentRepository
    {
        public StudentRepository(PrekvalifikacijaDBEntities context) : base(context)
        {

        }

        public List<Student> Get()
        {
            List<Student> students = context.Student.ToList();
            return students;
        }

        public Student GetByID(int id)
        {
            Student student = context.Student.FirstOrDefault(s => s.Id == id);
            if (student != null)
            {
                return student;
            }
            else
            {
                return null;
            }
        }

        public void Post(Student student)
        {
            context.Student.Add(student);
            context.SaveChanges();
        }

        public Student Put(int id, Student student)
        {
            Student result = context.Student.FirstOrDefault(s => s.Id == id);

            if (result != null)
            {
                result.Ime = student.Ime;
                result.Prezime = student.Prezime;
                result.BrojIndeksa = student.BrojIndeksa;
                result.GodinaStudija = student.GodinaStudija;

                context.SaveChanges();
                return result;
            }
            else
            {
                return null;
            }
        }

        public bool Delete(int id)
        {
            Student student = context.Student.FirstOrDefault(s => s.Id == id);

            if (student != null)
            {
                context.Student.Remove(student);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}