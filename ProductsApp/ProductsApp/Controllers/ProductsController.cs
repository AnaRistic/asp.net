﻿using ProductsApp.Models;
using ProductsApp.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProductsApp.Controllers
{
    public class ProductsController : ApiController
    {
        private readonly IProductRepository _productRepository;

        public ProductsController(IProductRepository productRepository)
        {
            this._productRepository = productRepository;
        }

        public List<Product> Get()
        {
            return this._productRepository.Get();
        }

        [HttpGet, Route("api/product/{id}")]
        public HttpResponseMessage Get(int id)
        {
            var product = this._productRepository.GetById(id);

            if (product != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, product);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Product with id = " + id + " does not exist");
            }
        }

        [HttpPost, Route("api/product")]
        public HttpResponseMessage Post(Product product)
        {
            try
            {
                this._productRepository.Post(product);

                var message = Request.CreateResponse(HttpStatusCode.Created, product);
                message.Headers.Location = new Uri(Request.RequestUri + product.Id.ToString());
                return message;
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut, Route("api/product/{id}")]
        public HttpResponseMessage Put(int id, Product product)
        {
            try
            {
                if (this._productRepository.Put(id, product) != null)
                {
                    Product result = this._productRepository.Put(id, product);
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Product with id = " + id + " does not exist");
                }
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpDelete, Route("api/product/{id}")]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                if (this._productRepository.Delete(id) == true)
                {
                    this._productRepository.Delete(id);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Product with id = " + id + " does not exist");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
