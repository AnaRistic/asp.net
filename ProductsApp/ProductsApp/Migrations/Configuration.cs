namespace ProductsApp.Migrations
{
    using ProductsApp.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ProductsApp.Models.ProductDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ProductsApp.Models.ProductDBContext context)
        {
            context.Product.AddOrUpdate(i => i.Id,
                new Product
                {
                    Id = 1,
                    Name = "Tomato Soup",
                    Category = "Groceries",
                    Price = 1
                },
                new Product
                {
                    Id = 2,
                    Name = "Yo-yo",
                    Category = "Toys",
                    Price = 3.75M
                },
                new Product
                {
                    Id = 3,
                    Name = "Hammer",
                    Category = "Hardware",
                    Price = 16.99M
                }
            );
        }
    }
}
