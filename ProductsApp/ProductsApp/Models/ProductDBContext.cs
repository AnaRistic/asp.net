﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ProductsApp.Models
{
    public class ProductDBContext : DbContext
    {
        public DbSet<Product> Product { get; set; }
    }
}