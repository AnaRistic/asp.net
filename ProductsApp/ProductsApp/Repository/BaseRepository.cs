﻿using ProductsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductsApp.Repository
{
    public class BaseRepository
    {
        protected readonly ProductDBContext context;

        public BaseRepository(ProductDBContext context)
        {
            this.context = context;
        }
    }
}