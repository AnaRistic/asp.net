﻿using ProductsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductsApp.Repository.IRepository
{
    public interface IProductRepository
    {
        List<Product> Get();
        Product GetById(int id);
        void Post(Product product);
        Product Put(int id, Product product);
        bool Delete(int id);
    }
}
