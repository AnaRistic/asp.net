﻿using ProductsApp.Models;
using ProductsApp.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductsApp.Repository
{
    public class ProductRepository : BaseRepository, IProductRepository
    {
        public ProductRepository(ProductDBContext context) : base(context)
        {

        }

        public void Post(Product product)
        {
            context.Product.Add(product);
            context.SaveChanges();
        }

        public bool Delete(int id)
        {
            Product product = context.Product.FirstOrDefault(p => p.Id == id);

            if (product != null)
            {
                context.Product.Remove(product);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Product> Get()
        {
            List<Product> products = context.Product.ToList();
            return products;
        }

        public Product GetById(int id)
        {
            Product product = context.Product.FirstOrDefault(p => p.Id == id);
            if (product != null)
            {
                return product;
            }
            else
            {
                return null;
            }
        }

        public Product Put(int id, Product product)
        {
            Product model = context.Product.FirstOrDefault(p => p.Id == id);

            if(model != null)
            {
                model.Id = product.Id;
                model.Category = product.Category;
                model.Name = product.Name;
                model.Price = product.Price;

                context.SaveChanges();
                return model;
            }
            else
            {
                return null;
            }
        }
    }
}