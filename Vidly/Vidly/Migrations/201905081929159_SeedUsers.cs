namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
            INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'481f611b-e744-4c3a-8493-28fe87804870', N'guest@vidly.com', 0, N'AG/NfI21OP9veKgUIKn+IuYQfdt+RyRCXjALd4p0P48wBD+/KHRkIvzq2tFZnuiIEA==', N'0e587122-d7b1-4a7d-a41a-dafd7c7e7476', NULL, 0, 0, NULL, 1, 0, N'guest@vidly.com')
            INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'55898c3c-1425-4601-8721-98fb1bcf39cc', N'admin@vidly.com', 0, N'ABbu+VQ72YMPc9PilpmMwX4mJASDWo12p90LITD17aZ5tWiESliR/f4NBR6N1Y8CEg==', N'dc446165-2567-48bc-b748-2c2bbe959d51', NULL, 0, 0, NULL, 1, 0, N'admin@vidly.com')
            
            INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'4d30fce2-736a-42b4-8245-743ceeb52453', N'CanManageMovies')
            
            INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'55898c3c-1425-4601-8721-98fb1bcf39cc', N'4d30fce2-736a-42b4-8245-743ceeb52453')
            "); 
        }
        
        public override void Down()
        {
        }
    }
}
